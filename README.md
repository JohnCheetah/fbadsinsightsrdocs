[![Developped by Cheetah Mobile](http://www.mobyaffiliates.com/wp-content/uploads/2015/10/12.png)](http://www.cmcm.com/en-us/)

# fbAdsInsightsR Documentation #

Documentation for [fbAdsInsightsR](https://bitbucket.org/JohnCheetah/fbadsinsightsr/src) package.

### Contents ###

Download or clone.

- [Vignette](https://bitbucket.org/JohnCheetah/fbadsinsightsr/downloads).
- [Manual](https://bitbucket.org/JohnCheetah/fbadsinsightsr/downloads).
- [Inner-workings](https://bitbucket.org/JohnCheetah/fbadsinsightsr/downloads) technical details (for contributors).

### How to view? ###

The pdf file(s) can be downloaded from the download section, to view the html files [download](https://bitbucket.org/JohnCheetah/fbadsinsightsrdocs/src) or clone the repository.

### Contributors ###

* John Coene <john.coene@cmcm.com> (author)
* GaoCong <gaocong1@cmcm.com>  (maintainer)